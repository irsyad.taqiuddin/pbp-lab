import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var home;
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.indigo),
        home: Scaffold(
          body: ListView(
            children: [
              Container(
                color: Colors.white,
                child: const Image(
                  image: AssetImage("images/logowhite.jpg"),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                color: Colors.indigo,
                child: const Text(
                  "Learn Wihout Limits",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                padding: const EdgeInsets.all(50),
                color: Colors.indigo,
                child: const Text(
                  "In the new normal era, distance and time often become a barrier for us to gain knowledge, thus we need a distance learning breakthrough that allows us to continue to gain knowledge without the barriers of space and time. We provides everything you need for learning in this style.",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                  textAlign: TextAlign.justify,
                ),
              ),
              Container(
                  padding: const EdgeInsets.all(50),
                  color: Colors.white,
                  child: const Text(
                    "We Collaborate with 200+ leading universities and schools",
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: const EdgeInsets.all(50),
                  color: Colors.indigo,
                  child: const Text(
                    "Who is the user?",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  padding: const EdgeInsets.all(40),
                  color: Colors.indigo,
                  child: const Text(
                    "Eduspace is a learning website aimed at the general public. In accordance with the purpose of Eduspace itself, namely \"Education for everyone\". So any school student, whether at the elementary, junior high, and high school levels who want to study according to the school curriculum and college students with their respective majors can create a user account on Eduspace and make Eduspace their facility in learning.",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.justify,
                  )),
              Container(
                  padding: const EdgeInsets.all(40),
                  color: Colors.indigo,
                  child: const Text(
                    "so what are you waiting for? come and join us today!",
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  )),
              Container(
                color: Colors.indigo,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.blue,
                        onPressed: () {},
                        child: const Text(
                          "JOIN NOW",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ]),
              )
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.business),
                label: 'Programs',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.school),
                label: 'Account',
              ),
            ],
          ),
        ));
  }
}
