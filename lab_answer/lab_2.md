1. Apakah perbedaan antara JSON dan XML?
    - JSON hanyalah format yang ditulis dalam javascript, sedangkan XML   adalah bahasa markup yang memiliki tag untuk mendefinisikan elemen
    - JSON cenderung lebih cepat karena ukuran file yang kecil, sedangkan XML cenderung lebih lambat karena memiliki ukuran file yang besar.
    - XML mendukung banyak tipe data kompleks, sedangkan JSON hanya mendukung string, angka, array Boolean, dan objek yang hanya berisi tipe primitif.

2. Apakah perbedaan antara HTML dan XML?
    - XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
    - XML merupakan bahasa yang memperhatikan Case Sensitive atau biasa kita sebut dengan bahasa yang peka pada huruf besar dan kecil tiap huruf dan kalimatnya, sedangkan HTML tidak peka dengan Case Sensitive atau besar dan kecil tiap huruf dan kalimatnya.
    - XML sangat ketat dalam memperhatikan tag penutup, sedangkan HTML tidak terlalu ketat pada tag penutup.