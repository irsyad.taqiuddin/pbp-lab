import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.purple,
          title: const Text('Eduspace'),
        ),
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'EDUSPACE',
                      style: TextStyle(
                          color: Colors.purple,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User Name',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: () {
                    //forgot password screen
                  },
                  textColor: Colors.purple,
                  child: const Text('Forgot Password'),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.purple,
                      child: const Text('Login'),
                      onPressed: () => submit(
                        context,
                        nameController.text,
                        passwordController.text,
                      ),
                    )),
                Container(
                    child: Row(
                  children: <Widget>[
                    const Text('Does not have account?'),
                    // ignore: deprecated_member_use
                    FlatButton(
                      textColor: Colors.purple,
                      child: const Text(
                        'Sign in',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        //signup screen
                      },
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ))
              ],
            )));
  }
}

//referensi: https://www.codepolitan.com/membuat-alert-di-flutter
void submit(BuildContext context, String email, String password) {
  if (email.isEmpty || password.isEmpty) {
    const snackBar = SnackBar(
      duration: Duration(seconds: 5),
      content: Text("User Name dan Password harus diisi"),
      backgroundColor: Colors.purple,
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    return;
  }

  AlertDialog alert = AlertDialog(
    title: const Text("Login Berhasil"),
    content: const Text("Selamat Anda Berhasil login"),
    actions: [
      TextButton(
        child: const Text(
          'Ok',
          style: TextStyle(color: Colors.purple),
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
    ],
  );

  showDialog(context: context, builder: (context) => alert);
  return;
}
