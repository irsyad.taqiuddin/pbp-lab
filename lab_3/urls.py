from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'),
    path('add_friend', add_friend, name='add_friend')
]