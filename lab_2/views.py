from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.

def index(request):
    note = Note.objects.all()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab2.html', response)

def xml(request):
    note = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(note, content_type="application/xml")

def json(request):
    note = serializers.serialize('json', Note.objects.all())
    return HttpResponse(note, content_type="application/json")