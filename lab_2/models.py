from django.db import models

# Create your models here.
class Note(models.Model):
    TO = models.CharField(max_length=30)
    FROM = models.CharField(max_length=30)
    TITLE = models.CharField(max_length=30)
    MESSAGE = models.TextField()